# Hibernate Assignment 1

Contains a package `hibertest` containing the following classes:
- `User` : Contains the basic data for a User relation
- `UserDAO` : Implements a Data Access Object that interacts with the Database

It also contains a servlet named `UserController` that faciltates the MVC architecture through a JSP page `index.jsp` that can allow the addition and listing of `User` records.

Note that the project files are loose and not ordered in a runnable way because this project was uploaded from my personal PC, that does not have the facilities to actually run it (like Eclipse, Tomcat, etc).