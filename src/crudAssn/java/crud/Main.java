import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        UserService userService = new UserService(sessionFactory);

        // create a new user
        User user = new User("John Doe", "johndoe@email.com");
        userService.createUser(user);

        // retrieve the user
        User retrievedUser = userService.getUser(user.getId());
        System.out.println("Retrieved user: " + retrievedUser);

        // update the user
        retrievedUser.setName("Jane Doe");
        userService.updateUser(retrievedUser);

        // retrieve the updated user
        User updatedUser = userService.getUser(retrievedUser.getId());
        System.out.println("Updated user: " + updatedUser);

        // delete the user
        userService.deleteUser(updatedUser.getId());

        // try to retrieve the deleted user
        User deletedUser = userService.getUser(updatedUser.getId());
        if (deletedUser == null) {
            System.out.println("User with ID " + updatedUser.getId() + " not found");
        }

        sessionFactory.close();
    }
}
