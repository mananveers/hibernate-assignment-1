package crud;

import java.util.List;

public class UserService {

    private UserDAO userDAO;
    
    public UserService() {
        userDAO = new UserDAO();
    }

    public void persist(User user) {
        userDAO.openCurrentSessionwithTransaction();
        userDAO.persist(user);
        userDAO.closeCurrentSessionwithTransaction();
    }

    public void update(User user) {
        userDAO.openCurrentSessionwithTransaction();
        userDAO.update(user);
        userDAO.closeCurrentSessionwithTransaction();
    }

    public User findById(String id) {
        userDAO.openCurrentSession();
        User user = userDAO.findById(id);
        userDAO.closeCurrentSession();
        return user;
    }

    public void delete(String id) {
        userDAO.openCurrentSessionwithTransaction();
        User user = userDAO.findById(id);
        userDAO.delete(user);
        userDAO.closeCurrentSessionwithTransaction();
    }

    public List<User> findAll() {
        userDAO.openCurrentSession();
        List<User> users = userDAO.findAll();
        userDAO.closeCurrentSession();
        return users;
    }

    public void deleteAll() {
        userDAO.openCurrentSessionwithTransaction();
        userDAO.deleteAll();
        userDAO.closeCurrentSessionwithTransaction();
    }

    public UserDAO userDAO() {
        return userDAO;
    }
}
