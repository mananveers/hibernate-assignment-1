<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>User Management</title>
</head>
<body>
    <h1>User Management</h1>
    <form action="<%= request.getContextPath() %>/users" method="post">
        <p>
            <label>Name:</label> <input type="text" name="name">
        </p>
        <p>
            <label>Email:</label> <input type="text" name="email">
        </p>
        <p>
            <input type="submit" value="Add User">
        </p>
    </form>

    <br>
    <br>

    <%
    List<User> listUser = (List<User>) request.getAttribute("listUser");
    if (listUser != null) {
        out.println("<table>");
        out.println("<tr>");
        out.println("<th>ID</th>");
        out.println("<th>Name</th>");
        out.println("<th>Email</th>");
        out.println("</tr>");
        for (User user : listUser) {
            out.println("<tr>");
            out.println("<td>" + user.getId() + "</td>");
            out.println("<td>" + user.getName() + "</td>");
            out.println("<td>" + user.getEmail() + "</td>");
            out.println("</tr>");
        }
        out.println("</table>");
    }
%>

</body>
</html>
